
import UIKit
import SpriteKit
import AVFoundation

class FireworxView: UIView {

    private let backgroundGradient = CAGradientLayer()
    private let spriteKitView = SKView(frame: UIScreen.mainScreen().bounds)
    private let spriteKitScene = SKScene(size: UIScreen.mainScreen().bounds.size)
    private let groundColor = UIColor(red: 0.3, green: 0.1, blue: 0.1, alpha: 1.0)
    private let maximumScreenDimension = max(UIScreen.mainScreen().bounds.size.width,
                                             UIScreen.mainScreen().bounds.size.height)
    private let gradientData = [ 0.0 : UIColor(hue: 240/360, saturation: 1.00, brightness: 0.12, alpha: 1.0).CGColor,
                                 0.4 : UIColor(hue: 225/360, saturation: 0.93, brightness: 0.29, alpha: 1.0).CGColor,
                                 0.7 : UIColor(hue: 220/360, saturation: 0.61, brightness: 0.33, alpha: 1.0).CGColor,
                                 0.9 : UIColor(hue: 354/360, saturation: 0.30, brightness: 0.44, alpha: 1.0).CGColor,
                                 1.0 : UIColor(hue:   5/360, saturation: 0.70, brightness: 0.50, alpha: 1.0).CGColor ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundGradient.frame = bounds
        spriteKitView.frame = bounds
        spriteKitScene.size = bounds.size
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        touches.forEach { self.createFireworkForLocation($0.locationInView(self)) }
    }
    
    private func setup() {
        srand48(time(UnsafeMutablePointer<Int>(bitPattern: 0)))
        multipleTouchEnabled = true
        
        spriteKitScene.backgroundColor = UIColor.clearColor()
        spriteKitScene.physicsWorld.gravity = CGVector(dx: 0, dy: -1)
        spriteKitView.allowsTransparency = true
        spriteKitView.presentScene(spriteKitScene)
        addSubview(spriteKitView)
        
        let ground = SKSpriteNode(color: groundColor, size: CGSize(width: maximumScreenDimension, height: 2.0))
        ground.position = CGPoint(x: maximumScreenDimension / 2.0, y: 1.0)
        ground.physicsBody = SKPhysicsBody(edgeFromPoint: CGPoint(x:-maximumScreenDimension, y:1.0),
                                           toPoint: CGPoint(x:maximumScreenDimension, y:1.0))
        spriteKitScene.addChild(ground)
        
        backgroundGradient.colors = [UIColor(hue: 240/360, saturation: 1.00, brightness: 0.12, alpha: 1.0).CGColor,
                                     UIColor(hue: 225/360, saturation: 0.93, brightness: 0.29, alpha: 1.0).CGColor,
                                     UIColor(hue: 220/360, saturation: 0.61, brightness: 0.33, alpha: 1.0).CGColor,
                                     UIColor(hue: 354/360, saturation: 0.30, brightness: 0.44, alpha: 1.0).CGColor,
                                     UIColor(hue:   5/360, saturation: 0.70, brightness: 0.50, alpha: 1.0).CGColor]
        backgroundGradient.locations = [0.0, 0.4, 0.7, 0.9, 1.0]
        layer.insertSublayer(backgroundGradient, atIndex: 0)
    }
    
    private func createFireworkForLocation(location:CGPoint) {
        guard let mortar = SKEmitterNode.emitterNodeNamed("Spark") else { return }
        AVAudioPlayer.playSoundNamed("whoosh.aif")
        
        mortar.particleBirthRate = 40
        mortar.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: 3, height: 3))
        mortar.targetNode = spriteKitScene
        mortar.position = CGPoint(x: bounds.size.width / 2, y: 4)
        spriteKitScene.addChild(mortar)
        
        let xImpulse:CGFloat = ((location.x / bounds.size.width) * 0.2) - 0.1
        let yMultiplier:CGFloat = bounds.size.height / 667.0
        let yImpulse:CGFloat = 0.2 * (1.0 - (location.y / bounds.size.height))
        mortar.physicsBody?.applyImpulse(CGVector(dx: xImpulse, dy: yImpulse * yMultiplier))
        
        let delay:NSTimeInterval = (drand48() * 2.0) + 1.0
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delay * NSTimeInterval(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            mortar.endAndRemove()
            AVAudioPlayer.playSoundNamed("boom.aif")
            self.createSmokeAtPosition(mortar.position)
            self.createExplosionAtPosition(mortar.position)
        }

    }
    
    private func createSmokeAtPosition(position:CGPoint) {
        guard let smoke = SKEmitterNode.emitterNodeNamed("Smoke") else { return }
        smoke.particleBirthRate = 0
        smoke.position = position
        spriteKitScene.addChild(smoke)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * NSTimeInterval(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            smoke.particleBirthRate = 300
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * NSTimeInterval(NSEC_PER_SEC))), dispatch_get_main_queue()) {
                smoke.endAndRemoveAfter(10)
            }
        }
    }
    
    private func createExplosionAtPosition(position:CGPoint) {
        guard let explosion = SKEmitterNode.emitterNodeNamed("Explosion") else { return }
        explosion.particleColorSequence = nil
        explosion.particleColor = UIColor(hue: CGFloat(drand48()), saturation: 0.5, brightness:0.8, alpha: 1.0)
        explosion.particleBirthRate = 2500
        explosion.position = position
        spriteKitScene.addChild(explosion)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * NSTimeInterval(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            explosion.endAndRemove()
        }
    }
}

extension SKEmitterNode {
    
    class func emitterNodeNamed(name:String) -> SKEmitterNode? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(NSBundle.mainBundle().pathForResource(name, ofType: "sks")!) as? SKEmitterNode
    }
    
    func endAndRemove() {
        endAndRemoveAfter(2)
    }
    
    func endAndRemoveAfter(duration:NSTimeInterval) {
        particleBirthRate = 0
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(duration) * Int64(NSEC_PER_SEC)), dispatch_get_main_queue()) {
            self.removeFromParent()
        }
    }
}

extension AVAudioPlayer {
    
    class func playSoundNamed(fileName:NSString) {
        guard let player = try? AVAudioPlayer(contentsOfURL: NSURL(string: NSBundle.mainBundle().pathForResource(fileName.stringByDeletingPathExtension, ofType: fileName.pathExtension)!)!) else { return }
        player.prepareToPlay()
        player.play()
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(player.duration) * Int64(NSEC_PER_SEC)), dispatch_get_main_queue()) {
            player.stop()
        }
    }
}